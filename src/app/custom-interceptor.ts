import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';


import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs';



@Injectable()
export class CustomInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authReq = req.clone({

            headers: req.headers.set('Access-Control-Allow-Origin', '*')
                .append('Access-Control-Allow-Methods', 'PATCH, GET, POST, PUT, DELETE, OPTIONS')
                .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method")
        });
        return next.handle(authReq);
    }
}