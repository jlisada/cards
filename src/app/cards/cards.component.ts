import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as xml2js from 'xml2js';
import * as $ from 'jquery';




@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  numCard: number = 4;


  selectedIndex: number = -1;
  isActive: number = -1;

  setSelected(id: number, elem: any) {
    if (this.isActive === -1) {
      if (this.selectedIndex === -1) {
        this.selectedIndex = id;
        this.isActive = id

      }

    } else if (this.isActive === id) {
      if (this.selectedIndex === id) {
        this.selectedIndex = -1;
        this.isActive = -1

      }
    }
  }

  createRange() {
    var items: number[] = [];
    for (var i = 1; i <= this.numCard; i++) {
      items.push(i);
    }
    return items;
  }

  obj: any[] = [];

  getcatPictures() {
    var obj2;
    this.http.get('http://thecatapi.com/api/images/get?format=xml&results_per_page='+ this.numCard, {responseType: 'text'})
      .subscribe(data => {
        xml2js.parseString(data, function (err, result) {
          obj2 = result.response.data[0].images[0].image;
        });
        this.obj = obj2;
      });
  }
 
  addcard(){
    this.numCard = this.numCard + 1;
    this.createRange();
  }

  imgcontent(index){
    if(this.obj.length == 0 || this.obj[index-1] == null){
      return "";
    }
    return this.obj[index-1].url[0];
  }
}
